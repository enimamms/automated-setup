## Automated Setups Usage


The script has only been tested on Ubuntu server 20.04, so this version is recommended.

Due to the version of glibc, the binary can only run on ubuntu with glibc 2.31.

1. [Download Ubuntu Server](https://ubuntu.com/download/server) and install Ubuntu Server
1. Clone this project to your server /root folder and `cd` to this project.
1. Copy your world\_\<wordname\>\_privatedata to the ~ directory (The directory will be deleted automatically after the script is executed)  
1. Execute `NAME=VALUE bash ubuntu-setup` (Please refer to the beginning of the script for available variable names)

Example: 

```
ROOTUSER=on WORLD_DOWNLOAD_DOMAIN="yourdomain" WORLD_NAME=yourworld WORLD_GIT="https://gitlab.com/yourworld" PORT=30000 MOTD="Welcome to my server" OWNER=yourname STATIC_SPAWNPOINT="1000,15,1000" bash ubuntu-setup
```

If you created a user instead of using the root user you should set ROOTUSER=off, and world\_\<wordname\>\_privatedata is also in this user directory.

## Compile your binary (minetestserver)

[Refer to this page ](https://github.com/minetest/minetest/tree/stable-5#compiling)

Example:

```
sudo apt install g++ make libc6-dev libirrlicht-dev cmake libbz2-dev libpng-dev libjpeg-dev libxxf86vm-dev libgl1-mesa-dev libsqlite3-dev libogg-dev libvorbis-dev libopenal-dev libcurl4-gnutls-dev libfreetype6-dev zlib1g-dev libgmp-dev libjsoncpp-dev libluajit-5.1-2 libpq-dev libleveldb-dev libspatialindex-dev
cmake . -DRUN_IN_PLACE=FALSE -DBUILD_SERVER=TRUE -DBUILD_CLIENT=FALSE -DCMAKE_BUILD_TYPE=Release
make -j$(nproc)
```

## License

Source code license: GPLv3 (see file LICENSE)

/installation_files same as minetest license

/patches -> See License.txt inside
